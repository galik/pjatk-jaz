package servlets;

import services.Calculations;
import services.Loan;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by galik on 05.03.2016.
 */
@WebServlet("/harmonogram")
public class HarmonogramServlet extends HttpServlet {

    Loan loan;
    Calculations calculations;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String kwota = request.getParameter("kwota");
        String okres = request.getParameter("okres");
        String oprocentowanie = request.getParameter("oprocentowanie");
        String oplata = request.getParameter("oplata");
        String raty = request.getParameter("raty");

        String validationMessage = validateData(response, request, kwota, oprocentowanie, oplata);

        if (!validationMessage.equals("")) {
            request.setAttribute("message", validationMessage);
            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp?");
            rd.forward(request, response);
        } else {
            loan = new Loan(kwota, okres, oprocentowanie, oplata);
            calculations = new Calculations(loan);

            response.setContentType("text/html");
            PrintWriter writer = response.getWriter();

            if (raty.equals("malejace")) {
                writer.println("Malejace");
                printDecreasingRates(writer);
            } else if (raty.equals("stale")) {
                writer.println("Stale");
                printConstantRates(writer);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String message = request.getAttribute("message").toString();
        request.getRequestDispatcher("/").include(request, response);
    }

    private String validateData(HttpServletResponse response, HttpServletRequest request, String kwota, String oprocentowanie, String oplata) throws IOException, ServletException {
        String message = "";
        if (kwota.equals("")) {
            message += "Pole Kwota nieprawidłowe<br>";
        }
        if (oprocentowanie.equals("")) {
            message += "Pole Oprocentowanie nieprawidłowe<br>";
        }
        if (oplata.equals("")) {
            message += "Pole Opłata nieprawidłowe<br>";
        }
        return message;
    }

    private void printDecreasingRates(PrintWriter writer) {
        writer.println("<table><tr>"
                +"<th>Nr raty</th>"
                +"<th>Kwota Kapitału</th>"
                +"<th>Kwota odsetek</th>"
                +"<th>Pozostały kapitał</th>"
                +"<th>Całkowita kwota raty</th>"
                +"</tr>");
        for (Integer i = 0; i < loan.getOkres(); i++) {
            Map<String, Double> Rate = calculations.calculateDecreasingRate().get(i);
            writer.print("<tr>"
                    +"<td>"+(i + 1)+"</td>"
                    +"<td>"+Rate.get("CapitalPart")+"</td>"
                    +"<td>"+Rate.get("Interest")+"</td>"
                    +"<td>"+Rate.get("Capital")+"</td>"
                    +"<td>"+Rate.get("Rate")+"</td>"
                    +"</tr>");
        }
        writer.println("</table>");
    }

    private void printConstantRates(PrintWriter writer) {
        writer.println("<table><tr>"
                +"<th>Nr raty</th>"
                +"<th>Kwota Kapitału</th>"
                +"<th>Kwota odsetek</th>"
                +"<th>Pozostały kapitał</th>"
                +"<th>Całkowita kwota raty</th>"
                +"</tr>");

        for (Integer i = 1; i <= loan.getOkres(); i++) {
            Map<String, Double> Rate = calculations.calculateConstantRate().get(i);
            writer.print("<tr>"
                    +"<td>"+i+"</td>"
                    +"<td>"+Rate.get("CapitalPart")+"</td>"
                    +"<td>"+Rate.get("Interest")+"</td>"
                    +"<td>"+Rate.get("Capital")+"</td>"
                    +"<td>"+Rate.get("Rate")+"</td>"
                    +"</tr>");
        }
        writer.println("</table>");
    }
}
