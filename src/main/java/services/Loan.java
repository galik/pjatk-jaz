package services;

/**
 * Created by galik on 05.03.2016.
 */
public class Loan {

    private Integer kwota;
    private Integer okres;
    private Double oprocentowanie;
    private Integer oplata;

    public Integer getKwota() {
        return kwota;
    }

    public void setKwota(Integer kwota) {
        this.kwota = kwota;
    }

    public Integer getOkres() {
        return okres;
    }

    public void setOkres(Integer okres) {
        this.okres = okres;
    }

    public Double getOprocentowanie() {
        return oprocentowanie;
    }

    public void setOprocentowanie(Integer oprocentowanie) {
        this.oprocentowanie = (double) oprocentowanie / 100;
    }

    public Integer getOplata() {
        return oplata;
    }

    public void setOplata(Integer oplata) {
        this.oplata = oplata;
    }

    public Loan(String kwota,
                String okres,
                String oprocentowanie,
                String oplata) {
        this.kwota = Integer.parseInt(kwota);
        this.okres = Integer.parseInt(okres);
        this.oprocentowanie = (double) Integer.parseInt(oprocentowanie) / 100;
        this.oplata = Integer.parseInt(oplata);
    }
}
