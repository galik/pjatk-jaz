package services;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by galik on 19.03.2016.
 */
public class Calculations {

    private Loan loan;
    
    public Calculations(Loan loan) {
        this.loan = loan;
    }

    private Double round(Double number) {
        return Math.round(number * 100d) / 100d;
    }

    public Map<Integer, Map<String, Double>> calculateDecreasingRate() {
        Map<Integer, Map<String, Double>> Rates = new HashMap<Integer, Map<String, Double>>();
        Double CapitalPart = (double) (this.loan.getKwota() + this.loan.getOplata()) / this.loan.getOkres();
        Double RestOfCapital = (double) (this.loan.getKwota() + this.loan.getOplata());
        Double Interest;

        for (Integer i = 0; i <= this.loan.getOkres(); i++) {
            Map<String, Double> rata = new HashMap<String, Double>();
            Interest = (RestOfCapital*this.loan.getOprocentowanie())/this.loan.getOkres();
            rata.put("Rate", this.round(CapitalPart));
            rata.put("Capital", this.round(RestOfCapital));
            rata.put("CapitalPart", this.round((CapitalPart - Interest)));
            rata.put("Interest", this.round(Interest));
            Rates.put(i, rata);
            RestOfCapital -= CapitalPart;
        }

        return Rates;
    }

    public Map<Integer, Map<String, Double>> calculateConstantRate() {
        Map<Integer, Map<String, Double>> Rates = new HashMap<Integer, Map<String, Double>>();
        Double Capital = (double) this.loan.getKwota() + this.loan.getOplata();
        Double RestOfCapital = Capital;
        Double Rate = (Capital * Math.pow((1 + this.loan.getOprocentowanie()), this.loan.getOkres())) * (this.loan.getOprocentowanie() / (Math.pow((1 + this.loan.getOprocentowanie()), this.loan.getOkres()) - 1));
        Double Interest = Capital * this.loan.getOprocentowanie();
        for (Integer i = 1; i <= this.loan.getOkres(); i++) {
            Map<String, Double> rata = new HashMap<String, Double>();
            rata.put("Rate", this.round(Rate));
            rata.put("Capital", this.round(RestOfCapital));
            rata.put("CapitalPart", this.round(Rate - Interest));
            rata.put("Interest", this.round(Interest));
            RestOfCapital = (Capital * Math.pow((1 + this.loan.getOprocentowanie()), i)) - (Rate * ((Math.pow((1 + this.loan.getOprocentowanie()), i) - 1)/this.loan.getOprocentowanie()));
            Interest = RestOfCapital * this.loan.getOprocentowanie();
            Rates.put(i, rata);
        }

        return Rates;
    }
}
