<html>
  <head>
    <title>WebApp</title>
  </head>
  <body>
  <p>${message}</p>
  <form action="/harmonogram" method="post">

    <label for="kwota">Wnioskowana kwota kredytu:</label>
    <input type="number" id="kwota" name="kwota"/>

    <label for="okres">Okres kredytowania:</label>
    <select id="okres" name="okres">
      <option value="3">3</option>
      <option value="6">6</option>
      <option value="12">12</option>
      <option value="24">24</option>
      <option value="36">36</option>
      <option value="48">48</option>
    </select>

    <br>

    <label for="oprocentowanie">Oprocentowanie:</label>
    <input type="number" maxlength="2" id="oprocentowanie" name="oprocentowanie"/>%

    <br>

    <label for="oplata">Oplata stala:</label>
    <input type="number" id="oplata" name="oplata"/>

    <br>

    <fieldset>
      <legend>Raty:</legend>
      <input type="radio" name="raty" value="malejace" checked="checked"> Malejace
      <input type="radio" name="raty" value="stale" > Stale
    </fieldset>

    <br>

    <input type="submit" value="Wejdz"/>

  </form>
  </body>
</html>
