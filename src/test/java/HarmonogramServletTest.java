import org.junit.Test;
import org.mockito.Mockito;
import services.Calculations;
import services.Loan;
import servlets.HarmonogramServlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by galik on 19.03.2016.
 */
public class HarmonogramServletTest extends Mockito {

    @Test
    public void servlet_should_forward_if_kwota_is_null() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher rd = mock(RequestDispatcher.class);
        PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
        when(request.getRequestDispatcher("/index.jsp?")).thenReturn(rd);
        HarmonogramServlet servlet = new HarmonogramServlet();

        when(request.getParameter("kwota")).thenReturn("");
        when(request.getParameter("okres")).thenReturn("10");
        when(request.getParameter("oprocentowanie")).thenReturn("10");
        when(request.getParameter("oplata")).thenReturn("10");

        servlet.doPost(request, response);

        verify(rd).forward(request, response);
    }

    @Test
    public void servlet_should_forward_if_oprocentowanie_is_null() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher rd = mock(RequestDispatcher.class);
        PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
        when(request.getRequestDispatcher("/index.jsp?")).thenReturn(rd);
        HarmonogramServlet servlet = new HarmonogramServlet();

        when(request.getParameter("kwota")).thenReturn("10");
        when(request.getParameter("okres")).thenReturn("10");
        when(request.getParameter("oprocentowanie")).thenReturn("");
        when(request.getParameter("oplata")).thenReturn("10");

        servlet.doPost(request, response);

        verify(rd).forward(request, response);
    }

    @Test
    public void servlet_should_forward_if_oplata_is_null() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher rd = mock(RequestDispatcher.class);
        PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
        when(request.getRequestDispatcher("/index.jsp?")).thenReturn(rd);
        HarmonogramServlet servlet = new HarmonogramServlet();

        when(request.getParameter("kwota")).thenReturn("10");
        when(request.getParameter("okres")).thenReturn("10");
        when(request.getParameter("oprocentowanie")).thenReturn("10");
        when(request.getParameter("oplata")).thenReturn("");

        servlet.doPost(request, response);

        verify(rd).forward(request, response);
    }

}
