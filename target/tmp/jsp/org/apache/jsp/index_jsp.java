package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\n");
      out.write("  <head>\n");
      out.write("    <title>WebApp</title>\n");
      out.write("  </head>\n");
      out.write("  <body>\n");
      out.write("  <p>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${message}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</p>\n");
      out.write("  <form action=\"/harmonogram\" method=\"post\">\n");
      out.write("\n");
      out.write("    <label for=\"kwota\">Wnioskowana kwota kredytu:</label>\n");
      out.write("    <input type=\"number\" id=\"kwota\" name=\"kwota\"/>\n");
      out.write("\n");
      out.write("    <label for=\"okres\">Okres kredytowania:</label>\n");
      out.write("    <select id=\"okres\" name=\"okres\">\n");
      out.write("      <option value=\"3\">3</option>\n");
      out.write("      <option value=\"6\">6</option>\n");
      out.write("      <option value=\"12\">12</option>\n");
      out.write("      <option value=\"24\">24</option>\n");
      out.write("      <option value=\"36\">36</option>\n");
      out.write("      <option value=\"48\">48</option>\n");
      out.write("    </select>\n");
      out.write("\n");
      out.write("    <br>\n");
      out.write("\n");
      out.write("    <label for=\"oprocentowanie\">Oprocentowanie:</label>\n");
      out.write("    <input type=\"number\" maxlength=\"2\" id=\"oprocentowanie\" name=\"oprocentowanie\"/>%\n");
      out.write("\n");
      out.write("    <br>\n");
      out.write("\n");
      out.write("    <label for=\"oplata\">Oplata stala:</label>\n");
      out.write("    <input type=\"number\" id=\"oplata\" name=\"oplata\"/>\n");
      out.write("\n");
      out.write("    <br>\n");
      out.write("\n");
      out.write("    <fieldset>\n");
      out.write("      <legend>Raty:</legend>\n");
      out.write("      <input type=\"radio\" name=\"raty\" value=\"malejace\" checked=\"checked\"> Malejace\n");
      out.write("      <input type=\"radio\" name=\"raty\" value=\"stale\" > Stale\n");
      out.write("    </fieldset>\n");
      out.write("\n");
      out.write("    <br>\n");
      out.write("\n");
      out.write("    <input type=\"submit\" value=\"Wejdz\"/>\n");
      out.write("\n");
      out.write("  </form>\n");
      out.write("  </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
